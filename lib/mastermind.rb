
p 'MY CODE!!!'

class Code
  attr_reader :pegs
  PEGS = {'R' => :red,
          'G' => :green,
          'B' => :blue,
          'Y' => :yellow,
          'O' => :orange,
          'P' => :purple
  }


  def self.random #this is a factory method/class method
    set = []
    4.times {set << PEGS.values.sample} #interesting way to create random sequence
    #sample method selects one item from an array, no need for combo
    Code.new(set) #create new instance, feed it back NOTICE!!!
  end

  def self.parse(input) #this is also a factory method/class method

    raise 'Error, must select four colors' unless input.length == 4
    #this returns an array of symbols from the input string
    colors = input.upcase.split('').map do |letter|
      #rejects invalid INTERESTING
      raise 'Error, not valid input' unless PEGS.has_key?(letter.upcase)
      #replaces with symbols
      PEGS[letter.upcase]
    end
    Code.new(colors) #create new instance, feed it back NOTICE!!!!!
  end

  def initialize(pegs)
    @pegs = pegs
  end

  def exact_matches(other_code) #whole object
    count = 0
    pegs.each_index do |i|
      count += 1 if self[i] == other_code.pegs[i]
    end
    count
  end

  def near_matches(other_code)
    other_color_counts = other_code.color_counts

    near_matches = 0
    self.color_counts.each do |color, count|
      next unless other_color_counts.has_key?(color)

      # Give credit for near matches up to `count`
      near_matches += [count, other_color_counts[color]].min
    end

    near_matches - self.exact_matches(other_code)
  end

  def color_counts
    color_counts = Hash.new(0)

    @pegs.each do |color|
      color_counts[color] += 1
    end

    color_counts
  end

  def [](i) #no clue what this does, copied this from the solution code
    pegs[i]
  end #for some reason even though instance#pegs is an array, cant access index unless this is written


  def ==(other_code) #this method is able to compare the pegs var within each instance without having to access it everytime
    return false unless other_code.is_a?(Code) #rejects if different class

    self.pegs == other_code.pegs #here, compares pegs with pegs
  end


end





class Game
  attr_reader :secret_code

  #Essentially, 1 this class creates a CODE instance with a random set of pegs,
  #2 gets inputs and creates anohter CODE instance
  #compares those two code instances,
  # 3 this GAME class really only handles the game flow, doesnt interact all with pegs or colors


  def initialize(secret_code = Code.random) #creates random
    @secret_code = secret_code
  end

  def play
    @turns_left = 10
    until @turns_left == 0
      @guess = get_guess
      return winner_message if @secret_code==(@guess) #see line 56
      @turns_left -= 1
      p "Wrong!"
      display_matches(@guess)
      p "You have #{@turns_left} chances left"
    end
    return loser_message
  end

  def get_guess
    p 'Hey whats your guess?'
    guess = gets.chomp #gets input from player
    Code.parse(guess)# this creates an instance with the input as a pegs var
  end

  def display_matches(guess)
    #refer to code class
    exact_match_count = @secret_code.exact_matches(guess)
    near_match_count = @secret_code.near_matches(guess)
    p "You have exactly #{exact_match_count} match(es)"
    p "You have #{near_match_count} near matches"
     #this refers to the actual pegs of the guess
  end


  def winner_message
    p 'Congratulations! You win!'
  end

  def loser_message
    p 'You lose boss'
  end

end
